package cn.sarvjeet.ds;
import java.util.Scanner;

 class Source {
     public static void main(String[] args) {
         Scanner s = new Scanner(System.in);

         int n1 = s.nextInt();
         int[] array1 = new int[n1];
         for (int i = 0; i < n1; i++) {
             array1[i] = s.nextInt();
         }

         int n2 = s.nextInt();
         int[] array2 = new int[n2];
         for (int i = 0; i < n2; i++) {
             array2[i] = s.nextInt();
         }

         int wrongIndex = 0;
         for (int i = 1; i < n1; i++) {
             if (array1[i] < array1[i - 1]) {
                 wrongIndex = i;

             }
         }
         int maximum =0;
         for (int i = 0; i < n2; i++)
         {
             if (array2[i] > maximum &&
                     array2[i] >= array1[wrongIndex - 1])
             {
                 if (wrongIndex + 1 <= n1 - 1 &&
                         array2[i] <= array1[wrongIndex + 1])
                 {
                     maximum = array2[i];


                 }
             }
         }
         if (maximum==0){
             System.out.println("Not Possible");

         }else {
             System.out.println(maximum);
         }

     }
 }


