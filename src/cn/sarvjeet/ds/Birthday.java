package cn.sarvjeet.ds;


/*
Rohit and Rahul are very close friends,
and as Rahul’s birthday is coming up,
Rohit decides to share a chocolate bar with him.
Rohit has made you the in-charge for the sharing of this chocolate.
Each square of the bar has a number written on it, and
Rohit has decided that he will share a contiguous piece of chocolate,
in which the sum of each number on the squares is equal to the month in which Rahul was born,
and the numbers written on these squares add up to Rahul’s birth date.
You need to find the number of ways the chocolate bar can be divided.
*/


import java.util.*;
public class Birthday {
    public static void main(String[] args) {
            Scanner s = new Scanner(System.in);
            int n = s.nextInt();
            int[] arr = new int[n];
            for(int i=0; i < n; i++){
                arr[i] = s.nextInt();
            }
            int d = s.nextInt();
            int m = s.nextInt();
            numberOfSubArrays(arr, n, d, m);
        }
        // Method to find number of subarrays of size m whose sum of elements is d
        static void numberOfSubArrays(int[] arr,int n, int d, int m){
            int count=0;
            for(int i =0;i<=n-m;i++){
                int sum =0;
                for(int j=i;j<i+m;j++){
                    sum+=arr[j];
                }
                if(sum==d)
                {
                    count ++;
                }
            }
            System.out.println(count);

        }
    }
